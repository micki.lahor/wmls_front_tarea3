'use client'
import React from 'react'
import Typography from '@mui/material/Typography'
import ArrowOutwardIcon from '@mui/icons-material/ArrowOutward'
import {
  Box,
  Button,
  Card,
  CardActions,
  CardContent,
  CardMedia,
  Grid,
} from '@mui/material'

export default function Servicios() {
  return (
    <Box
      display="flex"
      flexDirection="column"
      alignItems="static"
      sx={{ backgroundColor: '#f0f2f5', padding: '30px 30px' }}
    >
      <Typography variant="h6" fontWeight={'bold'} sx={{ mb: 4 }}>
        Nuestros Servicios
      </Typography>
      <Grid container spacing={5}>
        <Grid item xs={12} md={6} lg={3}>
          <Card sx={{ minWidth: 210, alignItems: 'center' }}>
            <CardMedia
              sx={{ display: 'flex', justifyContent: 'center', mb: 4, mt: 4 }}
            >
              <img
                src="/images/cosmetics.png"
                alt="Odontología Cosmética"
                style={{ width: '100px', height: '100px' }}
              />
            </CardMedia>
            <CardContent
              sx={{
                display: 'flex',
                justifyContent: 'center',
                textAlign: 'center',
              }}
            >
              <Typography
                variant="h6"
                fontWeight={'bold'}
                sx={{ mb: 6 }}
                style={{ height: '50px' }}
              >
                Odontologia Cosmetica
              </Typography>
            </CardContent>
            <CardActions
              sx={{
                display: 'flex',
                justifyContent: 'center',
                textAlign: 'center',
              }}
            >
              <Button
                sx={{ alignItems: 'center', textAlign: 'center' }}
                size="small"
              >
                Mas Información
                <ArrowOutwardIcon />
              </Button>
            </CardActions>
          </Card>
        </Grid>
        <Grid item xs={12} md={6} lg={3}>
          <Card sx={{ minWidth: 210 }}>
            <CardMedia
              sx={{ display: 'flex', justifyContent: 'center', mb: 4, mt: 4 }}
            >
              <img
                src="/images/restaurative.png"
                alt="Odontología Restaurativa"
                style={{ width: '100px', height: '100px' }}
              />
            </CardMedia>
            <CardContent
              sx={{
                display: 'flex',
                justifyContent: 'center',
                textAlign: 'center',
              }}
            >
              <Typography
                variant="h6"
                fontWeight={'bold'}
                sx={{ mb: 6 }}
                style={{ height: '50px' }}
              >
                Odontologia Restaurativa
              </Typography>
            </CardContent>
            <CardActions
              sx={{
                display: 'flex',
                justifyContent: 'center',
                textAlign: 'center',
              }}
            >
              <Button
                sx={{ alignItems: 'center', textAlign: 'center' }}
                size="small"
              >
                Mas Información
                <ArrowOutwardIcon />
              </Button>
            </CardActions>
          </Card>
        </Grid>
        <Grid item xs={12} md={6} lg={3}>
          <Card sx={{ minWidth: 210 }}>
            <CardMedia
              sx={{
                display: 'flex',
                justifyContent: 'center',
                mb: 4,
                mt: 4,
              }}
            >
              <img
                src="/images/pediatric.png"
                alt="Odontopediatría"
                style={{ width: '100px', height: '100px' }}
              />
            </CardMedia>
            <CardContent
              sx={{
                display: 'flex',
                justifyContent: 'center',
                textAlign: 'center',
              }}
            >
              <Typography
                variant="h6"
                fontWeight={'bold'}
                sx={{ mb: 6 }}
                style={{ height: '50px' }}
              >
                Odontopediatría
              </Typography>
            </CardContent>
            <CardActions
              sx={{
                display: 'flex',
                justifyContent: 'center',
                textAlign: 'center',
              }}
            >
              <Button
                sx={{ alignItems: 'center', textAlign: 'center' }}
                size="small"
              >
                Mas Información
                <ArrowOutwardIcon />
              </Button>
            </CardActions>
          </Card>
        </Grid>
        <Grid item xs={12} md={6} lg={3}>
          <Card sx={{ minWidth: 210 }}>
            <CardMedia
              sx={{
                display: 'flex',
                justifyContent: 'center',
                mb: 4,
                mt: 4,
              }}
            >
              <img
                src="/images/orthodontics.png"
                alt="Ortodoncia"
                style={{ width: '100px', height: '100px' }}
              />
            </CardMedia>
            <CardContent
              sx={{
                display: 'flex',
                justifyContent: 'center',
                textAlign: 'center',
              }}
            >
              <Typography
                variant="h6"
                fontWeight={'bold'}
                sx={{ mb: 6 }}
                style={{ height: '50px' }}
              >
                Ortodoncia
              </Typography>
            </CardContent>
            <CardActions
              sx={{
                display: 'flex',
                justifyContent: 'center',
                textAlign: 'center',
              }}
            >
              <Button
                sx={{ alignItems: 'center', textAlign: 'center' }}
                size="small"
              >
                Mas Información
                <ArrowOutwardIcon />
              </Button>
            </CardActions>
          </Card>
        </Grid>
      </Grid>
    </Box>
  )
}
