'use client'
import React from 'react'
import WhatsAppIcon from '@mui/icons-material/WhatsApp'
import FacebookRoundedIcon from '@mui/icons-material/FacebookRounded'
import YouTubeIcon from '@mui/icons-material/YouTube'
import InstagramIcon from '@mui/icons-material/Instagram'
import { Grid, IconButton, Typography } from '@mui/material'

export default function Contacto() {
  return (
    <Grid
      container
      alignItems="center"
      justifyContent="center"
      padding={2}
      style={{ backgroundColor: '#f0f2f5' }}
    >
      <Grid item xs={12} md={12} lg={12} sx={{ textAlign: 'center' }}>
        <Typography color="secondary" fontWeight="bold" sx={{ mb: 1 }} >
          Siguenos en redes sociales
        </Typography>
      </Grid>
      <Grid item xs={12} md={12} lg={12} sx={{ textAlign: 'center', mb: 1 }}>
        <IconButton
          aria-label="Twitter"
          href="https://www.whatsapp.com/"
          color="secondary"
          size="small"
        >
          <WhatsAppIcon />
        </IconButton>
        <IconButton
          aria-label="Instagram"
          href="https://www.instagram.com/"
          color="secondary"
          size="small"
        >
          <InstagramIcon />
        </IconButton>
        <IconButton
          aria-label="Facebook"
          href="https://www.facebook.com/"
          color="secondary"
          size="small"
        >
          <FacebookRoundedIcon />
        </IconButton>
        <IconButton
          aria-label="YouTube"
          href="https://www.youtube.com/"
          color="secondary"
          size="small"
        >
          <YouTubeIcon />
        </IconButton>
      </Grid>
      <Grid item xs={12} md={12} lg={12} sx={{ textAlign: 'center' }}>
        <Typography color="secondary" fontWeight="bold" sx={{ mb: 1 }} >
          Política de privacidad
        </Typography>
      </Grid>
    </Grid>
  )
}
