'use client'

import React, { PropsWithChildren } from 'react'
import { Container } from '@mui/material'
import Navbar from '../../components/Navbar'

const Layout: React.FC<PropsWithChildren> = ({ children }) => {
  return (
    <>
      <Navbar />
      <Container sx={{ p: 2 }}>{children}</Container>
    </>
  )
}

export default Layout
