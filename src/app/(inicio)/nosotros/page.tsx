'use client'
import React from 'react'
import Container from '@mui/material/Container'
import Typography from '@mui/material/Typography'
import { Box, Button } from '@mui/material'
import Image from 'next/image'

export default function Nosotros() {
  return (
    <Container>
      <Box
        display="flex"
        justifyContent="space-between"
        alignItems="center"
        padding={2}
        borderRadius={2}
        bgcolor="white"
      >
        <Box maxWidth="50%">
          <Typography variant="h6" color="textSecondary">
            Clínica odontológica especializada
          </Typography>
          <Typography variant="h4" component="h1" gutterBottom color="primary">
            Atención para todas las edades
          </Typography>
          <Button
            variant="contained"
            color="primary"
            size="large"
            style={{ borderRadius: '10px', fontWeight: 'bold' }}
          >
            Reserva en línea
          </Button>
          <Typography variant="body1" color="black" marginTop={2}>
            o llama al 800-10-01-02
          </Typography>
        </Box>
        <Box position="relative" width={350} height={350}>
          <Image
            src="/images/banner.png"
            alt="Dental Clinic"
            layout="fill"
            objectFit="contain"
          />
        </Box>
      </Box>
    </Container>
  )
}
