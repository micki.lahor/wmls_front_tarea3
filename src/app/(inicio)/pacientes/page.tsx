'use client'
import React from 'react'
import Typography from '@mui/material/Typography'
import {
  Box,
  Card,
  CardContent,
  Grid,
} from '@mui/material'

export default function Pacientes() {
  return (
    <Box
      display="flex"
      flexDirection="column"
      alignItems="static"
      sx={{ backgroundColor: "white", padding: "30px 30px" }}
    >
      <Typography
        variant="h6"
        fontWeight={'bold'}
        sx={{ mb: 4 }}
      >
        Opiniones de nuestros pacientes
      </Typography>
      <Grid container spacing={5}>
        <Grid item xs={12} md={6} lg={4}>
          <Card sx={{ height: 220 }}>
            <CardContent
              sx={{
                justifyContent: 'center',
                textAlign: 'left',
              }}
            >
              <Typography sx={{ mb: 4 }}>
                &quot;Excelente servicio en Brillo Dental, Trato amable,
                precedimientos indolores. ¡Mi sonrisa nunca ha estado
                mejor!&quot;
              </Typography>
              <Typography fontWeight={'bold'}>Lucia P.</Typography>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} md={6} lg={4}>
          <Card sx={{ height: 220 }}>
            <CardContent
              sx={{
                justifyContent: 'center',
                textAlign: 'left',
              }}
            >
              <Typography sx={{ mb: 4 }}>
                &quot;En Magia Dental me sentí como en casa, Profesionales
                excepcionales y resultados sorprendentes. ¡Muy
                recomendado!&quot;
              </Typography>
              <Typography fontWeight={'bold'}>Jorge E.</Typography>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} md={6} lg={4}>
          <Card sx={{ height: 220 }}>
            <CardContent
              sx={{
                justifyContent: 'center',
                textAlign: 'left',
              }}
            >
              <Typography sx={{ mb: 4 }}>
                &quot;Magia Dental transformo mi sonrisa con eficiencia y
                cuidado. ¡El mejor dentista al que he ido!&quot;
              </Typography>
              <Typography fontWeight={'bold'}>Sofia M.</Typography>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Box>

  )
}
